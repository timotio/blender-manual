.. index:: Tags

#################
  Extensions Tags
#################

This is the list of the tags currently supported:

* 3D View
* Add Mesh
* Add Curve
* Animation
* Compositing
* Development
* Game Engine
* Import-Export
* Lighting
* Material
* Modeling
* Mesh
* Node
* Object
* Paint
* Physics
* Render
* Rigging
* Scene
* Sequencer
* System
* Text Editor
* UV
* User Interface
