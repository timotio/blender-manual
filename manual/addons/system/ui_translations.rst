
**********************
Manage UI Translations
**********************

Todo


Activation
==========

- Open Blender and go to Preferences then the Add-ons tab.
- Click System then Manage UI translations to enable the script.


Description
===========

See `Blender translation guide <https://developer.blender.org/docs/handbook/translating/translator_guide/#manage-ui-translations-add-on>`__ in the Developer Handbook.


.. reference::

   :Category: System
   :Description: Allows managing UI translations directly from within Blender
                 (update main po-files, update scripts' translations, etc.).
   :Location: :menuselection:`Topbar --> File menu`, Text editor, any UI control
   :File: ui_translate folder
   :Author: Bastien Montagne
   :Note: This add-on is bundled with Blender.
